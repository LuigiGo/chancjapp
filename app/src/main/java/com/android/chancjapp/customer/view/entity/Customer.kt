package com.android.chancjapp.customer.view.entity

data class Customer(
    val firstname: String,
    val lastname: String
)