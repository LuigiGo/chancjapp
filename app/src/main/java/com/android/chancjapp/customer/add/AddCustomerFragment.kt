package com.android.chancjapp.customer.add

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import com.android.chancjapp.R
import com.android.chancjapp.databinding.FragmentAddCustomerBinding

class AddCustomerFragment : Fragment() {

    private lateinit var binding: FragmentAddCustomerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_add_customer, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
    }

    private fun setupViews() {
        binding.apply {
            with(btnAdd) {
                setOnClickListener {
                    val firstName = edtFirstname.text.toString()
                    val lastName = edtLastname.text.toString()

                    val action =
                        AddCustomerFragmentDirections.actionAddCustomerFragmentToCustomerProfileFragment(
                            firstName,
                            lastName
                        )
                    findNavController().navigate(action)
                }
            }
        }
    }
}